package me.lars.pce;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by MrLars_ on 10-8-2017.
 */
public class PlayerJoin implements Listener {

    public API api = new API();

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if(!ConfigManager.getInstance().hasAccount(player)) {
            api.createAccount(player);
        } else {
            return;
        }
    }
}
