package me.lars.pce;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by MrLars_ on 10-8-2017.
 */
public class API {

    /**
     * Creates an account for the specified player.
     * Returns nothing.
     */
    public void createAccount(Player player) {
        UUID uuid = player.getUniqueId();
        if (!ConfigManager.getInstance().hasAccount(player)) {
            ConfigManager.getInstance().getEconomyConfig().set(uuid.toString() + ".money", 0);
            ConfigManager.getInstance().getEconomyConfig().set(uuid.toString() + ".name", player.getName().toString());
            ConfigManager.getInstance().saveConfig();
        } else {
            ConfigManager.getInstance().getEconomyConfig().set(uuid.toString() + ".name", player.getName().toString());
            ConfigManager.getInstance().saveConfig();
        }
    }
        /**
         * Creates an account for the specified offline player.
         * Returns nothing.
         */
    public void createAccount(OfflinePlayer player) {
        UUID uuid = player.getUniqueId();
        if (!ConfigManager.getInstance().hasAccount(player)) {
            ConfigManager.getInstance().getEconomyConfig().set(uuid.toString() + ".money", 0);
            ConfigManager.getInstance().getEconomyConfig().set(uuid.toString() + ".name", player.getName().toString());
            ConfigManager.getInstance().saveConfig();
        } else {
            ConfigManager.getInstance().getEconomyConfig().set(uuid.toString() + ".name", player.getName().toString());
            ConfigManager.getInstance().saveConfig();
        }
    }

    /**
     * Checks if player has enough money to make the purchase.
     * Returns true if he has enough money, else false.
     */
    public boolean canWithdraw(Player player, Double v) {
        if (!ConfigManager.getInstance().hasAccount(player)) {
            createAccount(player);
            return false;
        } else if (v > 0) {
            if (ConfigManager.getInstance().getEconomyConfig().getDouble(player.getUniqueId().toString() + ".money") == v) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
    /**
     * Withdraws money out of a Player's bank account.
     * Make sure to check if they canWithdraw() first!
     */
    public void withdrawPlayer(Player player, Double v) {
        double old = ConfigManager.getInstance().getEconomyConfig().getDouble(player.getUniqueId().toString() + ".money");
        ConfigManager.getInstance().getEconomyConfig().set(player.getUniqueId().toString() + ".money", old - v);
        ConfigManager.getInstance().saveConfig();
    }
    /**
     * Withdraws money out of an OfflinePlayer's bank account.
     * Make sure to check if they canWithdraw() first!
     */
    public void withdrawPlayer(OfflinePlayer player, Double v) {
        double old = ConfigManager.getInstance().getEconomyConfig().getDouble(player.getUniqueId().toString() + ".money");
        ConfigManager.getInstance().getEconomyConfig().set(player.getUniqueId().toString() + ".money", old - v);
        ConfigManager.getInstance().saveConfig();
    }
    /**
     * Deposits money into an OfflinePlayer's bank account.
     * Make sure that they have an account first!
     */
    public void depositPlayer(Player player, Double v) {
        double old = ConfigManager.getInstance().getEconomyConfig().getDouble(player.getUniqueId().toString() + ".money");
        ConfigManager.getInstance().getEconomyConfig().set(player.getUniqueId().toString() + ".money", old + v);
        ConfigManager.getInstance().saveConfig();
    }
    /**
     * Deposits money into an OfflinePlayer's bank account.
     * Make sure that they have an account first!
     */
    public void depositPlayer(OfflinePlayer player, Double v) {
        double old = ConfigManager.getInstance().getEconomyConfig().getDouble(player.getUniqueId().toString() + ".money");
        ConfigManager.getInstance().getEconomyConfig().set(player.getUniqueId().toString() + ".money", old + v);
        ConfigManager.getInstance().saveConfig();
    }
    /**
     * Get balance of a Player.
     * Returns their balance.
     */
    public Double getBalance(Player player) {
        if(!ConfigManager.getInstance().hasAccount(player)) {
            createAccount(player);
        } else {
            return ConfigManager.getInstance().getEconomyConfig().getDouble(player.getUniqueId().toString() + ".money");
        }
        return 0.0;
    }
    /**
     * Get balance of an OfflinePlayer.
     * Returns their balance.
     */
    public Double getBalance(OfflinePlayer player) {
        if(!ConfigManager.getInstance().hasAccount(player)) {
            createAccount(player);
        } else {
            return ConfigManager.getInstance().getEconomyConfig().getDouble(player.getUniqueId().toString() + ".money");
        }
        return 0.0;
    }
    /**
     * Deposits money into an OfflinePlayer's bank account.
     * Make sure that they have an account first!
     */
    public void setPlayer(OfflinePlayer player, Double v) {
        ConfigManager.getInstance().getEconomyConfig().set(player.getUniqueId().toString() + ".money", v);
        ConfigManager.getInstance().saveConfig();
    }
}
