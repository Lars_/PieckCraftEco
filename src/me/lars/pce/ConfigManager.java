package me.lars.pce;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;


/**
 * Created by MrLars_ on 10-8-2017.
 */
public class ConfigManager {

    public static ConfigManager instance = new ConfigManager();

    private Plugin plugin = MainClass.getPlugin(MainClass.class);

    public FileConfiguration EconomyConfig;
    public File EconomyFile;

    public void setup() {
        if (!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdir();
        }

        EconomyFile = new File(plugin.getDataFolder(), "economy.yml");

        if (!EconomyFile.exists()) {
            try {
                EconomyFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        EconomyConfig = YamlConfiguration.loadConfiguration(EconomyFile);
    }

    public FileConfiguration getEconomyConfig() {
        return EconomyConfig;
    }

    public void saveConfig() {
        try {
            EconomyConfig.save(EconomyFile);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ConfigManager getInstance() {
        return instance;
    }

    public void reloadConfig() {
        EconomyConfig = YamlConfiguration.loadConfiguration(EconomyFile);
    }

    public boolean hasAccount(Player player) {
        if (getEconomyConfig().contains(player.getUniqueId().toString())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean hasAccount(OfflinePlayer player) {
        if (getEconomyConfig().contains(player.getUniqueId().toString())) {
            return true;
        } else {
            return false;
        }
    }
}