package me.lars.pce;

import net.md_5.bungee.api.ChatColor;

/**
 * Created by MrLars_ on 10-8-2017.
 */
public class Util {

    public static String Color(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }
}
