package me.lars.pce;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by MrLars_ on 10-8-2017.
 */
public class MainClass extends JavaPlugin {

    public static MainClass instance;

    @Override
    public void onEnable() {
        ConfigManager.getInstance().setup();
        loadConfig();
        instance = this;

        Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
        getCommand("pce").setExecutor(new EconomyCommand());
        getCommand("coins").setExecutor(new CoinsCommand());
    }

    private void loadConfig() {
        getConfig().options().copyDefaults(true);
        //add defaults
        getConfig().addDefault("no-permissions", "&cFout: U heeft hier geen permisie voor!");
        getConfig().addDefault("not-a-player", "&cFout: U bent geen speler!");
        getConfig().addDefault("not-enough-args", "&cFout: U heeft niet alle benodigde argumenten ingevuld!");
        getConfig().addDefault("player-does-not-exist", "&cFout: Speler niet gevonden!");
        getConfig().addDefault("amount-withdrawed", "&aU heeft succesvol %aantal% coins van de rekening van %speler% afgehaald.");
        getConfig().addDefault("amount-deposited", "&aU heeft succesvol %aantal% coins gestort op de rekening van %speler%");
        getConfig().addDefault("amount-set", "&aU heeft succesvol het aantal coins gezet op de rekening van %speler%");
        getConfig().addDefault("in-de-min", "&cFout: De speler heeft niet genoeg geld!");
        getConfig().addDefault("coins", "&aU heeft %aantal% coins op uw rekening!");
        getConfig().addDefault("coins-target", "&a%speler% heeft %aantal% coins op zijn rekening!");
        saveConfig();
    }

    public static MainClass getInstance() {
        return instance;
    }
}
