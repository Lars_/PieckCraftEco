package me.lars.pce;

import com.avaje.ebeaninternal.server.cluster.mcast.Message;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by MrLars_ on 11-8-2017.
 */
public class CoinsCommand implements CommandExecutor {

    public API api = new API();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(Util.Color(Messages.NOT_A_PLAYER));
            return false;
        }
        Player player = (Player) commandSender;
        if (args.length == 0) {
            player.sendMessage(Util.Color(Messages.COINS).replaceAll("%aantal%", String.valueOf(api.getBalance(player))));
            return false;
        } else if (args.length == 1) {
            OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
            if (target == null || !ConfigManager.getInstance().hasAccount(target)) {
                player.sendMessage(Util.Color(Messages.PLAYER_DOES_NOT_EXIST));
                return false;
            } else if (target != null) {
                player.sendMessage(Util.Color(Messages.COINS_TARGET).replaceAll("%aantal%", String.valueOf(api.getBalance(target))).replaceAll("%speler%", target.getName()));
            }
        }
        return false;
    }
}
