package me.lars.pce;

import sun.applet.Main;

/**
 * Created by MrLars_ on 10-8-2017.
 */
public class Messages {

    public static String NO_PERMISSIONS = MainClass.getInstance().getConfig().getString("no-permissions");
    public static String NOT_A_PLAYER = MainClass.getInstance().getConfig().getString("not-a-player");
    public static String NOT_ENOUGH_ARGS = MainClass.getInstance().getConfig().getString("not-enough-args");
    public static String PLAYER_DOES_NOT_EXIST = MainClass.getInstance().getConfig().getString("player-does-not-exist");
    public static String AMOUNT_WITHDRAWED = MainClass.getInstance().getConfig().getString("amount-withdrawed");
    public static String AMOUNT_DEPOSITED = MainClass.getInstance().getConfig().getString("amount-deposited");
    public static String AMOUNT_SET = MainClass.getInstance().getConfig().getString("amount-set");
    public static String IN_DE_MIN = MainClass.getInstance().getConfig().getString("in-de-min");
    public static String COINS = MainClass.getInstance().getConfig().getString("coins");
    public static String COINS_TARGET = MainClass.getInstance().getConfig().getString("coins-target");
}
