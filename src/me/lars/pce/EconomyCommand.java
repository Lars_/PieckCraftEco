package me.lars.pce;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by MrLars_ on 10-8-2017.
 */
public class EconomyCommand implements CommandExecutor {

    public API api = new API();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        Player player = (Player) commandSender;
        if(!player.hasPermission("pieckcrafteco.use")) {
        player.sendMessage(Util.Color(Messages.NO_PERMISSIONS));
        return false;
    }
        if(args.length < 3) {
            player.sendMessage(Util.Color(Messages.NOT_ENOUGH_ARGS));
            return false;
        }
        if(args.length == 3 && args[0].equalsIgnoreCase("withdraw")) {
            OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
            if(target == null) {
                player.sendMessage(Util.Color(Messages.PLAYER_DOES_NOT_EXIST));
                return false;
            } else if(target != null) {
                Double amount = Double.parseDouble(args[2]);
                if (ConfigManager.getInstance().getEconomyConfig().getDouble(player.getUniqueId().toString() + ".money") < amount) {
                    player.sendMessage(Util.Color(Messages.IN_DE_MIN));
                    return false;
                } else {
                    api.withdrawPlayer(target, amount);
                    player.sendMessage(Util.Color(Messages.AMOUNT_WITHDRAWED.replaceAll("%speler%", target.getName()).replaceAll("%aantal%", amount.toString())));
                    return false;
                }
            }
        }
        if(args.length == 3 && args[0].equalsIgnoreCase("deposit")) {
            OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
            if(target == null) {
                player.sendMessage(Util.Color(Messages.PLAYER_DOES_NOT_EXIST));
                return false;
            } else if(target != null) {
                Double amount = Double.parseDouble(args[2]);
                api.depositPlayer(target, amount);
                player.sendMessage(Util.Color(Messages.AMOUNT_DEPOSITED.replaceAll("%speler%", target.getName()).replaceAll("%aantal%", amount.toString())));
                return false;
            }
        }
        if(args.length == 3 && args[0].equalsIgnoreCase("set")) {
            OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
            if(target == null) {
                player.sendMessage(Util.Color(Messages.PLAYER_DOES_NOT_EXIST));
                return false;
            } else if(target != null) {
                Double amount = Double.parseDouble(args[2]);
                api.setPlayer(target, amount);
                player.sendMessage(Util.Color(Messages.AMOUNT_SET.replaceAll("%speler%", target.getName()).replaceAll("%aantal%", amount.toString())));
                return false;
            }
        }
        return false;
    }
}
